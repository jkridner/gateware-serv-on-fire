// runserv.c
// gcc runserv.c -o runserv
// MUST BE RUN FROM: sudo ./runserv blinky.hex

#include <stdio.h>    // printf
#include <stdlib.h>   // exit
#include <stdint.h>   // uint32_t
#include <stdbool.h>  // bool
#include <sys/mman.h> // mmap, munmap
#include <fcntl.h>    // O_RDWR|O_SYNC
#include <unistd.h>   // close mem_fd

// THIS IS THE BASE ADDRESS OF THE CAPE
#define CAPE_BASE      0x41000000
#define APBCSR_BASE    0x41000000
#define APBBLINKY_BASE 0x41010000
#define APBMEM_BASE    0x41020000
#define APBSERV_BASE   0x41040000
#define CAPE_SIZE 4096

bool handle_args(int argc, char *argv[], uint32_t *block_base, size_t *block_size, char **filename);
uint32_t *setup_hw(uint32_t hw_base, size_t nbytes);

int main(int argc, char **argv)
{
  uint32_t  block_base = 0x41040000;
  size_t    block_size = 64*1024;
  size_t    csr_offset = 1;
  char      *filename;

  bool err = handle_args(argc, argv, &block_base, &block_size, &filename);
  if (err) exit(1);

  printf("serv_base=0x%x, size=%ld, filename='%s'\n", block_base, block_size, filename);

  volatile uint32_t *csr_base_addr = setup_hw(APBCSR_BASE, block_size);
  volatile uint32_t *serv_base_addr = setup_hw(APBSERV_BASE, block_size);

  uint32_t csr = csr_base_addr[4];
  if (csr != 0) {
    printf("Warning: SERV should have been in reset, but was not\n");
  }
  
  csr_base_addr[4] = 0; // force serv to reset
  csr = csr_base_addr[4];
  if (csr&1 != 0) {
    printf("FAIL: SERV should be in reset, but isn't. csr = 0x%08x\n",csr);
  } else {
    printf("pass: SERV should be in reset, and is. csr = 0x%08x\n",csr);
  }

  printf("Loading %s into SERV\n", filename);  
  FILE* fileptr = fopen(filename, "r");
  if (fileptr == NULL) {
    printf("Error: could not open input data filename: '%s'\n", filename);
    exit(1);
  }
  uint32_t datval = 0;
  char str[64];
  for (unsigned offset=0; true; offset++) {
    if (fscanf(fileptr, "%s", str) != 1) break;
    datval = (uint32_t)strtol(str, NULL, 16);
    serv_base_addr[offset] = datval;
//  printf("mem[%02d] write = 0x%08x\n", offset, datval); fflush(stdout);
    printf("mem[%02d] write = 0x%08x, readback= 0x%08x\n", offset, datval, serv_base_addr[offset]);
  }
  fclose(fileptr);
  
  
  printf("Verify %s is in SERV\n", filename);  
  fileptr = fopen(filename, "r");
  if (fileptr == NULL) {
    printf("Error: could not open input data filename: '%s'\n", filename);
    exit(1);
  }
  str[64];
  uint32_t expect_val;
  uint32_t actual_val;
  for (unsigned offset=0; true; offset++) {
    if (fscanf(fileptr, "%s", str) != 1) break;
    expect_val = (uint32_t)strtol(str, NULL, 16);
    actual_val = serv_base_addr[offset];
    bool pass = expect_val == actual_val;
    if (pass) {
      printf("pass: ");
    } else {
      printf("FAIL: ");
    }
    printf("mem[%02d] actual = 0x%08x, exoect= 0x%08x\n", offset, actual_val, expect_val);
  }
  fclose(fileptr);
  
  printf("Take SERV out of reset\n");  
  csr_base_addr[4] = 0xffff;
  csr = csr_base_addr[4];
  if ((csr & 1) == 0) {
    printf("FAIL: SERV should not be in reset, but is. csr = 0x%08x\n",csr);
  } else {
    printf("pass: SERV should not be in reset, and is not. csr = 0x%08x\n",csr);
  }
  
  printf("SERV is now running program: %s\n", filename);  
  
  munmap((void *)csr_base_addr, block_size);
  munmap((void *)serv_base_addr, block_size);
  return 0;
} // main


// ============================================================================

bool handle_args(int argc, char *argv[], uint32_t *block_base, size_t *block_size, char **filename)
{
  bool err = false;
  if (argc < 2) {
    printf("Error. Missing arguments. Example: %s blinky.hex\n\n", argv[0]);
    return true;
  }

  *filename = argv[1];
  return err;
}

// ============================================================================

// Set up memory map region
uint32_t *setup_hw(uint32_t hw_base, size_t nbytes)
{
  int fd = open("/dev/mem", O_RDWR|O_SYNC);
  if (fd < 0) {
    printf("Error. Could not open /dev/mem. Try running with 'sudo'. \n");
    exit(-1);
  }

  void *mmem_base = mmap(NULL, nbytes, PROT_READ|PROT_WRITE, MAP_SHARED, fd, hw_base);

  close(fd);
  if (mmem_base == MAP_FAILED) {
    printf("Error: mmap failed\n");
    exit(-1);
  }
  return (uint32_t *)mmem_base;
} // setup_hw

